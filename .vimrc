" Vim-plug
call plug#begin('~/.vim/plugged')
    " Basic installation
    Plug 'tpope/vim-sensible'

    " Comment/uncomment lines
    Plug 'tpope/vim-commentary'

    " Shortcuts
    Plug 'tpope/vim-unimpaired'

    " Syntastic
    Plug 'scrooloose/syntastic'

    " NeoComplCache
    Plug 'Shougo/neocomplcache'
call plug#end()

" Syntastic tweaks
let g:syntastic_always_populate_loc_list = 1
autocmd filetype tex set noai nocin nosi inde=

" NeoComplCache tweaks
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'

" Syntax highlighting
syntax enable

" Colorscheme
set background=dark
set t_Co=256
" let g:solarized_termcolors=256
colorscheme solarized

" Source the vimrc file after saving it
augroup vimrc
    au!
    au bufwritepost ~/.vim/vimrc source $MYVIMRC
augroup END

" Replace tab by spaces
set tabstop=4 | set shiftwidth=4 | set expandtab
autocmd FileType html set tabstop=2 | set shiftwidth=2
autocmd FileType tex set tabstop=2 | set shiftwidth=2
autocmd FileType make setlocal noexpandtab

" Line number
set number

" More than 80 characters
augroup collumnLimit
  autocmd!
  autocmd BufEnter,WinEnter,FileType cpp
        \ highlight CollumnLimit ctermbg=Red guibg=Red
  let collumnLimit = 79 " feel free to customize
  let pattern =
        \ '\%<' . (collumnLimit+1) . 'v.\%>' . collumnLimit . 'v'
  autocmd BufEnter,WinEnter,FileType cpp
        \ let w:m1=matchadd('CollumnLimit', pattern, -1)
augroup END
